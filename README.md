# Ansible - Door To Darkness

## `setup.yml`
Creates my user, adds my SSH keys, and installs some general packages.

```shell
# If initializing a new machine, the root password must be provided
# since SSH keys will not exist
ansible-playbook -k setup.yml

# If ensuring configuration
ansible-playbook setup.yml
```

## Kubernetes
Kubernetes cluster related playbooks. See [its own README](./kubernetes/README.md) for more information.
