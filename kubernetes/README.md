# Ansible Playbooks for k3s Clusters

## pk3s - Production k3s
You will need SSH access as `$REMOTE_USERNAME` to basically every node in the cluster in order to use these playbooks.

### `pk3s_admin_localhost.yml`
Installs useful administration tools locally and authorizes the current user to the cluster.

#### Usage
```shell
ansible-playbook -kK -u $REMOTE_USERNAME pk3s_admin_localhost.yml
```

### `pk3s_deploy.yml`
Ensures the cluster is properly configured, whether starting from scratch or realigning the active cluster.

This playbook can also be used to upgrade the cluster.

**NOTE: k3s has initial support for running with SELinux enabled. At this time, it's broken on RHEL8. Environment variables are set in the used Ansible Roles to skip SELinux support.**

#### Usage
First, verify `hosts.yml` data.
```shell
ansible-playbook -kK -u $REMOTE_USERNAME pk3s_deploy.yml
```

### `pk3s_teardown.yml`
The nuclear option. Will destroy the entire cluster. As a safeguard, an additional prompt will confirm your decision.

Note that, ideally, the data is separated from the infrastructure and is at the very least backed up independently. In that case, running `pk3s_deploy.yml` can restore the infrastructure.
```shell
ansible-playbook -kK -u $REMOTE_USERNAME pk3s_teardown.yml
```
