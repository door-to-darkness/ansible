# ArgoCD
A declarative, GitOps continuous delivery tool for Kubernetes. We use this to deploy our applications to the cluster(s).

## Notes
- SSO is provided by GitLab for now
    - ArgoCD includes Dex as a shim for SSO using OIDC. [See here for how this was configured for gitLab.](https://argoproj.github.io/argo-cd/operator-manual/user-management/#dex)
        - `$dex.gitlab.clientSecret` refers to `argocd-secret.data.dex.gitlab.clientSecret`. `argo-secret` will already exist, so `dex.gitlab.clientSecret` needs to be added
    - [See here for information on configuring RBAC for the SSO accounts](https://argoproj.github.io/argo-cd/operator-manual/rbac/)
