# k3s Administrator Tools
Locally install useful administration tools for a Kubernetes server.

[[_TOC_]]

## Role Variables
- `k9s_version` - The version of k9s to install (default: v0.24.15)
- `kubectl_version` - The version of kubectl to install (default: v1.21.5)
- `kubectx_version`- The version of kubectx and kubens to install (default: v0.9.4)
- `pluto_version` - The version of pluto to install (default: 5.0.1)
- `popeye_version` - The version of popeye to install (default: v0.9.7)
- `rbac_tool_version` - The version of rbac-tool to install (default: v1.3.0)
- `velero_version` - The version of velero to install (default: v1.7.0)

## Example Playbook
```yaml
- hosts: localhost
  roles:
    - role: k3s_administrator_tools
```

## Included Tools
### helm
[Helm](https://helm.sh/) helps manage Kubernetes applications. Helm Charts define, install, and upgrade Kubernetes applications. Some of our applications are deployed using `helm`.

[Completion script](https://helm.sh/docs/helm/helm_completion/)

### k9s
[K9s](https://github.com/derailed/k9s) provides a terminal UI to interact with your Kubernetes clusters. `k9s` makes it easier to navigate, observe and manage applications. This can be a more pleasant experience than using `kubectl` directly.

### kubectl
[Kubectl](https://kubernetes.io/docs/reference/kubectl/overview/) is used to control Kubernetes clusters from the command line.

[Completion script](https://kubernetes.io/docs/reference/kubectl/cheatsheet/#kubectl-autocomplete)

### kubectx
[Kubectx](https://github.com/ahmetb/kubectx) contains both `kubectx` and `kubens`.
- `kubectx` is a utility to manage and switch between kubectl contexts
- `kubens` is a utility to switch between Kubernetes namespaces
These tools simplify working across many different namespaces and clusters and include autocompletion scripts which are immensely helpful.

[Completion script](https://github.com/ahmetb/kubectx#manual)

### pluto
[Pluto](https://github.com/FairwindsOps/pluto) is a utility to help users find deprecated Kubernetes apiVersions in their code repositories and their helm releases.

### popeye
[Popeye](https://github.com/derailed/popeye) is a utility that scans live Kubernetes cluster and reports potential issues with deployed resources and configurations.

### rbac-tool
[Rbac-tool](https://github.com/alcideio/rbac-tool) simplifies querying and creation RBAC policies. `rbac-tool` provides useful utilieis for ensuring best practices regarding access to Kubernetes clusters.

[Completion script](https://github.com/alcideio/rbac-tool#rbac-tool)

### velero
[Velero](https://github.com/vmware-tanzu/velero) gives you tools to back up and restore your Kubernetes cluster resources and persistent volumes.

[Completion script](https://velero.io/docs/v1.7/customize-installation/#install-bash-completion)

### yq
[Yq](https://github.com/mikefarah/yq) is a lightweight and portable command-line YAML processor. yq uses jq like syntax but works with yaml files as well as JSON. This is useful for manipulating the many YAML files in the Kubernetes space, especially if the related tool does not natively output JSON for manipulation with `jq`.
