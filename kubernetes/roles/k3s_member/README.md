# k3s Member
Join a node to an existing k3s cluster.

## Role Variables
- `k3s_become_master`: Whether or not to make the node a Master in the cluster (default: false)
- `k3s_datastore_endpoint`: The address of the datastore (default: '')
  - Only if not using embedded etcd
- `k3s_server`: The full address including port of the k3s server (default: '')
- `k3s_tls_san`: Add additional hostname or IP as a Subject Alternative Name in the TLS cert (default: '')
- `k3s_token`: The secret token used to join nodes to the cluster (default: '')
- `k3s_use_embedded_etcd`: Whether or not to use the embedded etcd system (default: false)
  - Only if not using external datastore
- `k3s_version`: The k3s version to install and use (default: v1.19.9+k3s1)

## Dependencies
- `k3s_dependencies`

## Example Playbook
```yaml

# Master - Using embedded etcd
- hosts: k3s_members
  become: yes
  vars_files:
    - ./external_vars.yml
  roles:
    - role: pk3s_member
      vars:
        k3s_become_master: true
        k3s_version: v1.19.9+k3s1
        k3s_server: https://{{ groups['k3s_primary'][0] }}:6443
        # k3s_token: Set in external_vars.yml
        k3s_use_embedded_etcd: true

# Master - Using external Postgres datastore
- hosts: k3s_members
  become: yes
  vars_files:
    - ./external_vars.yml
  roles:
    - role: pk3s_member
      vars:
        k3s_become_master: true
        k3s_version: v1.19.9+k3s1
        k3s_datastore_endpoint: "postgres://{{ postgres_user }}:{{ postgres_password }}@{{ groups['k3s_datastore'][0] }}:5432/k3s?sslmode=disable"
        # k3s_token: Set in external_vars.yml

# Worker
# Using embedded etcd
- hosts: k3s_workers
  become: yes
  vars_files:
    - ./external_vars.yml
  roles:
    - role: pk3s_member
      vars:
        k3s_become_master: false
        k3s_version: v1.19.9+k3s1
        k3s_server: https://{{ groups['k3s_primary'][0] }}:6443
        # k3s_token: Set in external_vars.yml
        k3s_use_embedded_etcd: true
```
