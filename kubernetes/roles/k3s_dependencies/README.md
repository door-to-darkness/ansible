# k3s Dependencies
Configure the minimum requirements and dependencies for a k3s node. The role is primarily used as a dependency role for `k3s_primary` and `k3s_member` and won't need to be manually run.

## Example Playbook
If you needed to configure only the dependencies, for whatever reason:
```yaml
- hosts: k3s_masters
  become: yes
  roles:
    - k3s_dependencies
```
