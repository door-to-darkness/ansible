# k3s Primary
Create a MetalLB load balanced k3s cluster.

## Role Variables
- `k3s_version`: The k3s version to install and use (default: v1.19.9+k3s1)
- `k3s_datastore_endpoint`: The address of the datastore (default: '')
  - Only if not using embedded etcd
- `k3s_token`: The secret token used to join nodes to the cluster (default: '')
- `k3s_tls_san`: Add additional hostname or IP as a Subject Alternative Name in the TLS cert (default: '')
- `k3s_use_embedded_etcd`: Whether or not to use the embedded etcd system (default: false)
  - Only if not using external datastore
- `metallb_version`: The version of MetalLB to install (default: v0.9.6)

## Dependencies
- `k3s_dependencies`

## Example Playbook
```yaml

# Using embedded etcd
- hosts: k3s_primary_master
  become: yes
  vars_files:
    - ./external_vars.yml
  roles:
    - role: k3s_primary
      vars:
        k3s_version: v1.18.8+k3s1
        # k3s_token: Set in external_vars.yml
        k3s_use_embedded_etcd: true
        metallb_version: v0.9.3

# Using external Postgres datastore
- hosts: k3s_primary_master
  become: yes
  vars_files:
    - ./external_vars.yml
  roles:
    - role: k3s_primary
      vars:
        k3s_version: v1.18.8+k3s1
        k3s_datastore_endpoint: "postgres://{{ postgres_user }}:{{ postgres_password }}@{{ groups['k3s_datastore'][0] }}:5432/k3s?sslmode=disable"
        # k3s_token: Set in external_vars.yml
        metallb_version: v0.9.3
```
