# Generate Cluster CSR
Generate a private key and a certificate signing request using `openSSL`.

## Role Variables
- `csr_path`: Path to store the generated `.csr` (default: "{{ workdir }}/{{ username }}{{ (k3s_environment) | ternary(k3s_environment_suffix, '') }}.csr")
- `k3s_environment`: Environment this certificate is for (default: "")
  - Used for file naming purposes only
- `k3s_environment_suffix`: Attach suffix to file name(default: -{{ lookup('vars', 'k3s_environment') }})
  - Used for file naming purposes only
- `key_path`: Path to store the generated `.key` (default: "{{ workdir }}/{{ username }}{{ (k3s_environment) | ternary(k3s_environment_suffix, '') }}.key")
- `username`: FSW username (default: "")
  - Used for file naming purposes only
- `workdir`: Directory to store the private key and csr (default: "~/.kube")

## Example Playbook
```yaml
- hosts: localhost
  roles:
    - role: generate_cluster_csr
      vars:
        username: jsmith
        k3s_environment: prod
```
