# Ansible Playbooks for Charon
Charon is the external loadbalancer. All HTTP and HTTPS traffic hits this first.

Charon runs DietPi for its operating system. SSH as `root` is disabled and `dietpi` is the only user account.

## `configure.yml`
Ensures `haproxy.cfg` matches this repo and restarts HAProxy if necessary.

### Usage
```shell
ansible-playbook -kK -u dietpi configure.yml
```
